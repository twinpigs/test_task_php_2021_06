<?php

namespace test2;

interface implementationServiceInterface
{
    public function getReportImplementationRate(int $report_id): ?array;
}